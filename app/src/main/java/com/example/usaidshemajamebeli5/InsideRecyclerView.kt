package com.example.usaidshemajamebeli5


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.usaidshemajamebeli5.databinding.ItemInsideLayoutBinding

class InsideRecyclerView(private val items: MutableList<ItemsModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return InsideItemViewHolder(ItemInsideLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is InsideItemViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = items.size

    inner class InsideItemViewHolder(private val binding: ItemInsideLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var item: ItemsModel
        fun bind() {
            item = items[adapterPosition]
            binding.editText.hint = item.hint
        }


    }
}