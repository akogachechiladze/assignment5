package com.example.usaidshemajamebeli5

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.usaidshemajamebeli5.databinding.ItemLayoutBinding

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    private val items = mutableListOf(mutableListOf<ItemsModel>())


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        // IDK :D
    }

    override fun getItemCount()= items.size

    inner class ItemViewHolder(private val binding: ItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        val recyclerView: RecyclerView = binding.recyclerViewInside
    }


    fun setItems(items: MutableList<MutableList<ItemsModel>>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}