package com.example.usaidshemajamebeli5

import com.google.gson.GsonBuilder
import okhttp3.ResponseBody

class ParseJSON {

    fun parseJSON(response: String?): ResponseBody? {
        val gson = GsonBuilder().create()
        return gson.fromJson(response, ResponseBody::class.java)
    }
}